semaine1: 

brainstorming : recherche d'idée

semaine2:

prototype/maquette

semaine3:

établir le code HTML de la page "Our practice " et la page "Contact" ( En fait , ils sont liés car la page "Our Practice " contient un bouton qui envoie
l'utlisateur vers le formulaire de la page "contact").

CODE HTML:

 <!-- main starts -->
    <main>
        <div class="wrapper">
            
            <section class="practiceContent container" id="about">
                <div class="practiceText">
                    <h2><span>Our practice</span> </h2>
                    <p>The practice of architecture consists of the provision of professional services in connection with town planning as well as the design, construction, enlargement, conservation, restoration, or alteration of a building or group of buildings. These professional services include, but are not limited to: planning and land-use planning,provision of preliminary studies, designs, models, drawings and specifications,monitoring of construction and project management .</p>
                    <button><a href="#contact">Send Message</a></button>
                </div>
                <div class="practice-assets">
                    <div class="practiceImg1">
                        <img src="./static/image1.jpg" alt="Kitchen">
                    </div>
                    <div class="practiceImg2">
                        <img src="./static/image2.jpg" alt="Kitchen2">
                    </div>
                </div>
            </section>
			
	<!-- contact section -->
        <section class="contactContainer" id="contact">
            <div class="wrapper">
                <h2><span>Tell us about your ideas</span> </h2>
                <p>Our entire team of architects is at your disposal for any information or project requests.</p>
                <form action="#" method="#" name="contact-info">
                    <div class="contactForm container">
                        <div class="nameForm container">
                            <label for="name">Name</label>
                            <input type="text" id="name" name="name">
                        </div>
                        <div class="emailForm container">
                            <label for="email">Email</label>
                            <input type="email" id="email" name="email">
                        </div>
                        <div class="message container">
                            <label for="message">Message</label>
                            <textarea id="message" name="message"></textarea>
                        </div>
                    </div>
                </form>

                <button type="submit">Send Message</button>
            </div>
        </section>
        <!-- contact ends -->		
        
semaine 4:

établir le code HTML du bas de page "Footer" et commencer le code CSS.

CODE HTML:

<div class="wrapper">
        <!-- footer -->
        <footer class="container">
            <div class="footerImg">
                <img src="./static/image9.jpg" alt="Meeting room ">
            </div>
            <div class="footerContent container">
                <div class="footerInfo">
                    <p class="logo">designey</p>
                    <div class="location">
                        <p>Rue gazala</p>
                        <p>Ariana_Tunis_Tunisie</p>
                    </div>
                    <div class="contactInfo">
                        
                        <p>info@designey-studio.com</p>
                    </div>
                </div>
                <nav class="footerNav container">
                    <div class="footer-list container">
                        <p>Menu</p>
                        <ul>
                            <li><a href="#">Services</a></li>
                            <li><a href="#">Who We Are</a></li>
                            <li><a href="#contact">Contact</a></li>
                        </ul>
                    </div>
                    <div class="footer-list container">
                        <p>Projects</p>
                        <ul>
                            <li><a href="#">Residential</a></li>
                            <li><a href="#">Commercial</a></li>
                            <li><a href="#">Conceptual</a></li>
                        </ul>
                    </div>
                    <div class="footer-list container">
                        <p>Follow</p>
                        <ul>
                            <li><a href="#">Instagram</a></li>
                            <li><a href="#">Youtube</a></li>
                            <li><a href="#">Houzz</a></li>
                        </ul>
                    </div>
                </nav>
            </div>
            <div class="contentEnd">
                <p>© École Supérieure des communications de Tunis 2021</p>
            </div>
        </footer>
        <!-- footer ends  -->
        
   CODE CSS:
   .navContent,
.headerLinks,
.practiceContent,
.contactForm,
.clientInfo,
footer.container,
.footerContent,
.footerNav {
    justify-content: space-between;
}

.headerContent {
    align-items: center;
}

.navContent {
    position: relative;
}

.headerLinks {
    width: 480px;
}

header {
    position: relative;
}

.badge {
    position: relative;
    top: -70px;
    left: 50%;
    margin-left: -70px;
}

.header-assets {
    width: 100%;
    position: relative;
}

.headerImg {
    height: 550px;
}

.practiceText {
    width: 45%;
    padding-right: 75px;
}

.practice-assets {
    width: 50%;
    position: relative;
}

.practiceText,
.practice-assets {
    height: 600px;
}

.practiceImg1,
.practiceImg2 {
    width: 60%;
    height: 75%;
}

.practiceImg1 {
    position: absolute;
    bottom: 0;
    z-index: 20;
}

.practiceImg2 {
    position: absolute;
    right: 0;
    z-index: 10;
}

.spotlight-assets img {
    padding: 10px;
}

.spotlight1,
.spotlight2 {
    width: 50%;
    height: 425px;
    position: relative;
}

.spotlight1 h3,
.spotlight2 h3 {
    position: absolute;
    right: 0;
    bottom: 10px;
}

.spotlight2 {
    width: calc(100%/3);
}

.practiceContent  button,
.contactContainer button {
   letter-spacing: 1px;
    }
.contactContainer {
    background-image: url(./static/image8.jpg);
    background-size: cover;
    padding: 50px 0 100px 0;
}

.contactForm {
    width: 60%;
    margin-top: 50px;
}

.contactForm input {
    height: 45px;
}

label {
    display: inline-block;
    padding: 10px 0;
}

input,
textarea {
    border: 1px solid #B18F39;
}

.nameForm {
    width: 45%;
}

.emailForm {
    width: 50%;
}

.message {
    width: 100%;
    margin-top: 10px;
}

textarea {
    resize: none;
    margin-bottom: 25px;
    height: 235px;
}

.clientText {
    width: 720px;
    height: auto;
    border-left: 5px solid #B18F39;
    padding-left: 35px;
    margin-top: 15px;
}

.clientInfo {
    position: relative;
}

.clientImgProfile {
    width: 100%;
}

.clientImg {
    width: 100px;
    height: 100px;
    border-radius: 50%;
}

.clientProfile {
    width: 35%;
    justify-content: flex-end;
    padding-left: 25px;
}
        
semaine 5:

Finir une partie du code CSS et se réunir avec l'équipe pour organiser le code complet .

CODE CSS:

@media (max-width: 675px) {
    h1 {
        font-size: 3.6rem;
        line-height: 55px;
        width: 100%;
    }

    .headerLinks {
        font-size: 1.6rem;
    }

    .headerImg {
        height: 500px;
    }

    .practiceImg1,
    .practiceImg2 {
        height: 65%;
    }

    .spotlight1,
    .spotlight2{
        width: 100%;
    }

    .spotlight1 h3,
    .spotlight2 h3 {
        width: 455px;
        margin: 0;
        bottom: 10px;
        left: 10px;
    }

    .spotlight1 {
        height: 375px;
    }

    .spotlight2 {
        height: 475px;
    }
    
    .spotlightContainer p,
    .contactContainer p {
        width: 75%;
    }

    .contactForm {
        flex-direction: column;
        width: 100%;
    }

    .nameForm,
    .emailForm {
        width: 100%;
    }
    .contactContainer button {
        width: 100%;
    }

    .arrowProfile {
        flex-direction: column;
    }

    .clientProfile {
        width: 100%;
    }
 
    .arrow {
        width: 125px;
        bottom: -30px;
    }

    footer {
        padding-top: 30px;
    }

    .footerContent {
        margin-top: 40px;
    }
}


@media (max-width: 480px) {
    .navContent {
        justify-content: center;
    }
    .headerLinks a {
        padding: 2px;
        font-size: 12px;
    }
    
    h1 {
        font-size: 2.8rem;
        line-height: 40px;
    }

    .headerImg {
        height: 400px;
    }

    h2 {
        font-size: 2.6rem;
    }
    

    .practiceContent  button,
    .contactContainer button {
        font-size: 1.4rem;
        width: 100%;
    }

    .practice-assets {
        height: 500px;
    }

    .spotlight1,
    .spotlight2 {
        height: 350px;
    }
    
    .spotlight1 h3,
    .spotlight2 h3 {
        width: 92.6%;
    }
    

    .spotlightContainer p,
    .contactContainer p {
        width: 100%;
    }

    .clientText p {
        font-size: 2rem;
        line-height: 35px;
    }

    .footerImg {
        height: 375px;
    }
    
    .footer-list li {
        padding-right: 35px;
    }

    .contentEnd p {
        font-size: 1.2rem;
    }
}
